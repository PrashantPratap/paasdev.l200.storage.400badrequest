﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaaSDev.L200.Storage._400BadRequest
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
        CloudConfigurationManager.GetSetting("StorageConnectionString"));

                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
                

                CloudBlockBlob blockBlob = container.GetBlockBlobReference("myblob");

                using (var fileStream = System.IO.File.OpenRead("PaaSDev.L200.Storage.400BadRequest.exe.config"))
                {
                    blockBlob.UploadFromStream(fileStream);
                }

                Console.Write("Code is White Monkey");
            }
            catch(Exception ex)
            {
                Console.Write(ex.ToString());
            }
        }
    }
}
